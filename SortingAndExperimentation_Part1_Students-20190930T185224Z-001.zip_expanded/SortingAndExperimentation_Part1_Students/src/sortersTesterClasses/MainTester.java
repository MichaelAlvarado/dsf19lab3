package sortersTesterClasses;

import java.util.Comparator;

import interfaces.Sorter;
import sorterClasses.BubbleSortSorter;
import sorterClasses.HeapSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;
import sortersTesterClasses.Entero;


public class MainTester {
	//Initialize Array for Testing
	static Entero[] entero = 
		{
				new Entero(2), new Entero(4), new Entero(1),
				new Entero(3), new Entero(9), new Entero(12),
				new Entero(17), new Entero(3), new Entero(9)
		};

	static Integer[] integer = new Integer[] {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
	static Integer[] integer2 = new Integer[] {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
	
	//Tester method
	public static void main(String[] args) { 
		Sorter <Entero> sorter = new HeapSortSorter<Entero>();
		Sorter <Integer> sorter1 = new InsertionSortSorter<Integer>();
		Sorter <Integer> sorter2 = new SelectionSortSorter<Integer>();

		//Sort the Entero Array using default comparator
		showArray("The unordered Array of 'Entero':", entero);
		sorter.sort(entero, null);	
		showArray("The ordered Array of 'Entero':",entero);

		System.out.println('\n');

		//Sort the Integer Array using IntegerComparator1
		showArray("The unordered Array of 'Integers':", integer);
		sorter1.sort(integer, new IntegerComparator1());
		showArray("The ordered Array of 'Integers':", integer);

		System.out.println('\n');

		//Sort the Integer Array using IntegerComparator2
		showArray("The unordered Array of 'Integers':", integer2);
		sorter2.sort(integer2, new IntegerComparator2());
		showArray("The ordered Array of 'Integers':", integer2);
	}

	private static void showArray(String message, Entero[] a) {
		System.out.println(message);
		for (int i=0; i<a.length; i++) 
			System.out.print("\t" + a[i].getValue()); 
		System.out.println();
	}
	private static void showArray(String message, Integer[] a) {
		System.out.println(message);
		for (int i=0; i<a.length; i++) 
			System.out.print("\t" + a[i].intValue()); 
		System.out.println();
	}
}